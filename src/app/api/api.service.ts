import {Observable} from 'rxjs';
import {HttpClient, HttpHandler} from '@angular/common/http';
import {Injectable} from '@angular/core';
// import {environment} from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ApiService extends HttpClient {

    constructor(handler: HttpHandler) {
        super(handler);
    }
    private _apiUrl = 'http://empresas.ioasys.com.br/api/v1';

    private buildUrl(endpoint): string {
        return [this._apiUrl, endpoint].join('/');
    }

    get(endpoint: string, options?: any | null): Observable<any> {
        return super.get(
            this.buildUrl(endpoint),
            options
        );
    }

    post(endpoint: string, body: any | null, options?: any | null): Observable<any> {
        return super.post(
            this.buildUrl(endpoint),
            body,
            options
        );
    }
}
