import {Component, ViewEncapsulation} from '@angular/core';
import { EnterpriseService } from '../../services/enterprise.service';

@Component({
  selector: 'app-enterprise',
  templateUrl: './enterprise.component.html',
  styleUrls: ['./enterprise.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class EnterpriseComponent {
  carregando = false;
  show = true;
  params = {name: ''};
  success = false;
  enterpriseList: any;
  showMore = false;
  clickedEnterprise: number;

  constructor(
    private _enterpriseService: EnterpriseService,
  ) {
    this.getEnterprises(this.params);
  }

  searchAction(event: any, params: any): void{
    event.stopPropagation();
    this.getEnterprises(params);
  }

  getEnterprises(params: any): void {
    this.carregando = true;
    this._enterpriseService.get(this.params)
      .subscribe((res: Response) => {
        this.carregando = false;
        this.success = true;
        this.enterpriseList = res.body['enterprises'];
      }, (error) => {
        this.carregando = false;
      });
  }
}
