import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { EnterpriseComponent } from './enterprise/enterprise.component';
import { AuthenticationService } from '../services/authentication.service';
import { EnterpriseService } from '../services/enterprise.service';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'enterprises',
    component: EnterpriseComponent
  },
  { path: '**', redirectTo: 'login' },
];

@NgModule({
  declarations: [
    LoginComponent,
    EnterpriseComponent
  ],
  providers: [
    AuthenticationService,
    EnterpriseService
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class RoutingModule { }
