import {Component, ViewEncapsulation} from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent {
  carregando = false;
  loginForm = {
    email: '',//'testeapple@ioasys.com.br',
    password: ''//'12341234'
  };
  constructor(
    private _authenticationService: AuthenticationService,
    private _router: Router,
  ) { }

  login(event: any, formLogin: any): void {
    event.stopPropagation();
    if (formLogin.valid) {
      this.carregando = true;
      this._authenticationService.login(this.loginForm.email, this.loginForm.password)
        .subscribe((res: Response) => {
          this.carregando = false;
          localStorage.setItem('access-token', res.headers.get('access-token'));
          localStorage.setItem('client', res.headers.get('client'));
          localStorage.setItem('uid', res.headers.get('uid'));
          this._router.navigate(['/enterprises']);
        }, (error) => {
          this.carregando = false;
          console.log(error);
        });
    }
  }
}
