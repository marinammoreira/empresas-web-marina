import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ApiService} from '../api/api.service';

@Injectable()
export class EnterpriseService {
  constructor (private _api: ApiService) {}

  get(params?: any | null): Observable<any> {
    const headers = {
      'access-token': localStorage.getItem('access-token'),
      'client': localStorage.getItem('client'),
      'uid': localStorage.getItem('uid')
    };
    return this._api.get('enterprises', {headers: headers, params: params, observe: 'response'});
  };

  getById(id: string): Observable<any> {
    const headers = {
      'access-token': localStorage.getItem('access-token'),
      'client': localStorage.getItem('client'),
      'uid': localStorage.getItem('uid')
    };
    return this._api.get(`enterprises${id}`, {headers: headers, observe: 'response'});
  };
}
