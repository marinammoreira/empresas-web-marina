import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ApiService} from '../api/api.service';

@Injectable()
export class AuthenticationService {
  constructor (private _api: ApiService) {}

  login(email: string, password: string): Observable<any> {
    const data = {
      email: email,
      password: password
    };
    return this._api.post('users/auth/sign_in', data, {observe: 'response'});
  }
}
